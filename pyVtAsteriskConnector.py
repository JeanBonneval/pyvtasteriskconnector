#! /usr/bin/python

from flask import Flask, request
import logging
import os,sys
import time
import shutil
from requests import HTTPError
from controllers.bridge import OriginateCall

#ajout des pythonpath
sys.path.insert(0,os.getcwd() + '/controllers')
sys.path.insert(0,os.getcwd() + '/models')

#logging configuration
FORMAT = '%(asctime)-15s %(funcName)s %(lineno)d  %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)

app = Flask(__name__)
@app.route('/makecall', methods=['GET','POST'])
def makecall():
	
	
    event = request.args.get('event')
    secret = request.args.get('secret')
    Caller = request.args.get('from')
    Callee = request.args.get('to')
    VtContext = request.args.get('context')
    
    if event == 'OutgoingCall':
		logging.info("Creating OriginateCall Object with param : {}, {}, {}, {}, {}".format('vt-call',VtContext, 'trunk_ovh', Caller, Callee))
		make_call_file(Caller, Callee, VtContext)
		call = OriginateCall('vt-call', VtContext, 'trunk_ovh', Caller, Callee)
		call.run()
		
    if request.method =='POST':
		return 'SUCCESS'

def make_call_file(caller, callee, context):
	
	logging.info("Enterring make_call_file() method")
	nomfic = "tmp/call_{}.call".format(time.time())
	callfile = open(nomfic,"w")
	callfile.write("Channel: SIP/{}\r\n".format(caller))
	callfile.write("CallerID: <{}>\r\n".format(caller))
	callfile.write("MaxRetries: 2\r\n")
	callfile.write("WaitTime: 20\r\n")
	callfile.write("Context: {}\r\n".format(context))
	callfile.write("Extension: {}\r\n".format(callee))
	callfile.write("Priority: 1\r\n".format(callee))
	callfile.close()
	shutil.copy(nomfic, '/var/spool/asterisk/outgoing/')
	

if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True)
