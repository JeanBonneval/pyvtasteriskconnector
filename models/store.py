#!/usr/bin/python
# -*- coding:utf-8 -*-

import time
import datetime
import logging


#logging configuration
FORMAT = '%(asctime)-15s %(funcName)s %(lineno)d  %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)

""" Class who store call, put the data call in Vtiger CRM."""


class StoreCall():
	
	def __init__(self, caller,callee, id_callee):
		
		self.caller = caller
		self.callee = callee
		self.caller_id = ''
		self.callee_id = id_callee
		self.date_of_call = datetime.datetime.now()

	def start_call(self):
		
		self.start_time = time.time()
		
	def end_call(self, state):
		self.end_time = time.time()
		self.duree = datetime.time.fromtimestamp(self.end_time - self.start_time)
		if state :
			self.state = 'Abouti'
		else:
			self.state = 'Non Abouti'
		
	def print_call_value(self):
		
		logging.info("---------log de l'appel-----------")
		logging.info("Appelant : {}".format(self.caller))
		logging.info("Appelé : {}".format(self.callee))
		logging.info("Etat de l'appel : {}".format(self.state))
		logging.info("Date de de l'appel : {}".format(self.date_of_call))
		logging.info("Durée : {}".format(self.duree))
		
		
		
