#!/usr/bin/python
# -*- coding:utf-8 -*-


import requests
import ari
import logging
from requests import HTTPError

#logging configuration
FORMAT = '%(asctime)-15s %(funcName)s %(lineno)d  %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)

""" Class who receive call, put the call in a holding bridge with Waiting Music.
    Then connect the two call together when Callee answerd"""

class OriginateCall():
	
	def __init__(self, appsname,context,trunk,caller,callee):
		#Connect to ARI
		self.client = ari.connect('http://10.0.72.246:8088', 'vtiger', '123456789')
		# Create a bridge, putting both channels into it.
		self.bridge = self.client.bridges.create(type='mixing')
		logging.info("Created bridge {}".format(self.bridge.id))
		#init incoming and outgoing channel
		self.caller = caller
		self.callee = callee
		self.incoming = ''
		self.outgoing = ''
		self.outgoing_endpoint=''
		self.name_apps = appsname
		self.context = context
		self.trunk = trunk

	def safe_hangup(self,channel):
		"""Hangup a channel, ignoring 404 errors.
		:param channel: Channel to hangup.
		"""
		try:
			channel.hangup()
		except HTTPError as e:
			# Ignore 404's, since channels can go away before we get to them
			if e.response.status_code != requests.codes.not_found:
				raise


	def on_start(self,channel, event):
		"""Callback for StasisStart events.
		When an incoming channel starts, put it in the holding bridge and
		originate a channel to connect to it. When that channel answers, create a
		bridge and put both of them into it.
		:param incoming:
		:param event:
		"""
		logging.info('----------Enterring on_start()----------')
		args = event.get('args')
		#Logging args
		logging.info("Logging args...")
		for arg in args:
			logging.info(arg)
		
		#do nothing if args[0] == dialed
		if args[0] == 'dialed' :
			return
		#formating endpoint args
		Callee = 'SIP/trunk_ovh/'+args[0]
		logging.info("Callee : {}".format(Callee))
		self.incoming = channel.get('channel')
		logging.info("Channel information...")
		for key, value in self.incoming.json.items():
			logging.info("{} : {}".format(key,value))				
		#Answer incoming channel
		self.incoming.answer()
		self.bridge.addChannel(channel=self.incoming.id)
		# Originate the outgoing channel
		self.outgoing = self.client.channels.originate(endpoint=Callee, app=self.name_apps, context='from_bibal', callerId=args[1], appArgs='dialed')
		logging.info('Outgoing channel id : {}'.format(self.outgoing.id))
		# If the incoming channel ends, hangup the outgoing channel
		self.incoming.on_event('StasisEnd', lambda *args: self.safe_hangup(self.outgoing))
		# and vice versa. If the endpoint rejects the call, it is destroyed
		# without entering Stasis()
		self.outgoing.on_event('ChannelDestroyed', lambda *args: self.safe_hangup(self.incoming))
		#launch outgoing call
		self.outgoing.on_event('StasisStart', self.outgoing_on_start)

	def outgoing_on_start(self,channel, event):
		"""Callback for StasisStart events on the outgoing channel
		:param channel: Outgoing channel.
		:param event: Event.
		"""
		logging.info('----------Enterring outgoing_on_start()----------')
		self.outgoing = channel.get('channel')
		self.outgoing.answer()
		logging.info('Outgoing channel id : {}'.format(self.outgoing.id))
		self.bridge.addChannel(channel=self.outgoing.id)
		# Clean up the bridge when done
		self.outgoing.on_event('StasisEnd', lambda *args: bridge.destroy())
		
	def run(self):
		#launch listner
		self.client.on_channel_event('StasisStart', self.on_start)
		#Run the WebSocket
		if self.name_apps != '':
			self.client.run(apps=self.name_apps)
		else:
			logging.error("name of apps not set")
		


def main():
	""" main programme """
	call = OriginateCall('vt-call')
	call.run()

if __name__ == '__main__':
    main()



