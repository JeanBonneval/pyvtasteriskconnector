#!/usr/bin/python
# -*- coding:utf-8 -*-


import requests
import ari
import logging
from models.store import StoreCall
from requests import HTTPError

#logging configuration
FORMAT = '%(asctime)-15s %(funcName)s %(lineno)d  %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)

""" Class who receive call, put the call in a holding bridge with Waiting Music.
    Then connect the two call together when Callee answerd"""

class OriginateCall():
	
	def __init__(self, appsname,context,trunk, incoming, outgoing):
		#Connect to ARI
		self.client = ari.connect('http://10.0.72.246:8088', 'vtiger', '123456789')
		# find or create a holding bridge
		bridges = [candidate for candidate in self.client.bridges.list() if
				   candidate.json['bridge_type'] == 'holding']
		if bridges:
			self.holdBridge = bridges[0]
			logging.info("Using Holding bridge {}".format(self.holdBridge.id))
			self.holdBridge.startMoh()
		else:
			self.holdBridge = self.client.bridges.create(type='holding')
			self.holdBridge.startMoh()
			logging.info("Created holding bridge {}".format(self.holdBridge.id))
		
		# Create a mixing bridge
		self.bridge = self.client.bridges.create(type='mixing')
		logging.info("Created Mixing bridge {}".format(self.bridge.id))
		#init incoming and outgoing channel
		self.callee = outgoing
		self.caller = incoming
		self.incoming = ''
		self.outgoing = ''
		self.outgoing_endpoint=''
		self.name_apps = appsname
		self.context = context
		self.trunk = trunk

	def safe_hangup(self,channel):
		"""Hangup a channel, ignoring 404 errors.
		:param channel: Channel to hangup.
		"""
		try:
			channel.hangup()
		except HTTPError as e:
			# Ignore 404's, since channels can go away before we get to them
			if e.response.status_code != requests.codes.not_found:
				raise


	def on_start(self,channel, event):
		"""Callback for StasisStart events.
		When an incoming channel starts, put it in the holding bridge and
		originate a channel to connect to it. When that channel answers, create a
		bridge and put both of them into it.
		:param incoming:
		:param event:
		"""
		logging.info('----------Enterring on_start()----------')
		args = event.get('args')
		#Logging args
		logging.info("Logging args...")
		for arg in args:
			logging.info(arg)
		
		#do nothing if args[0] == dialed
		if args[0] == 'dialed' :
			return
		self.incoming = channel.get('channel')
		self.incoming.answer()
		self.holdBridge.addChannel(channel=self.incoming.id)
		logging.info('Incoming channel id : {}'.format(self.incoming.id))
		#Creating model for call
		#self.call_model = StoreCall(self.caller,self.callee,self.incoming.id)
		#formating endpoint
		Callee = 'SIP/{}/{}'.format(self.trunk,self.callee)
		# Originate the outgoing channel
		self.outgoing = self.client.channels.originate(endpoint=Callee, app=self.name_apps, context=self.context, callerId=self.caller, appArgs='dialed')
		logging.info('Outgoing channel id : {}'.format(self.outgoing.id))
		# If the incoming channel ends, hangup the outgoing channel
		self.incoming.on_event('StasisEnd',lambda *args: self.safe_hangup(self.outgoing))
		# and vice versa. If the endpoint rejects the call, it is destroyed
		# without entering Stasis()
		self.outgoing.on_event('ChannelDestroyed', lambda *args: self.safe_hangup(self.incoming))
		#launch outgoing call
		self.outgoing.on_event('StasisStart', self.outgoing_on_start)

	def outgoing_on_start(self,channel, event):
		"""Callback for StasisStart events on the outgoing channel
		:param channel: Outgoing channel.
		:param event: Event.
		"""
		logging.info('----------Enterring outgoing_on_start()----------')
		self.outgoing = channel.get('channel')
		self.outgoing.answer()
		logging.info('Outgoing channel id : {}'.format(self.outgoing.id))
		#deplacement de l'appel depuis holding bridge vers mixing bridge
		self.holdBridge.removeChannel(channel=self.incoming.id)
		self.bridge.addChannel(channel=[self.outgoing.id, self.incoming.id])
		#Logging bridge
		#self.outgoing.on_event('ChannelStateChange',lambda *args: self.channel_state_change())
		# Clean up the bridge when done
		self.outgoing.on_event('StasisEnd', lambda *args: self.bridge.destroy())
	
	def channel_state_change(self,channel,event):
		
		"""Handler for changes in a channel's state"""
		logging.info('----------Enterring channel_state_change()----------')
		logging.info('Channel {} is now: {}'.format(channel.json.get('name'), channel.json.get('state')))
		logging.info('----------------------------------------------------')
		for key, value in channel.json.items():
			logging.info("{}: {}".format(key, value))
	
	def channel_end_logging(self, channel,event):
		
		"""handler for log the end of a channel"""
		logging.info('----------Enterring channel_end_logging()----------')
		logging.info('Channel {} is now: {}'.format(channel.json.get('name'), channel.json.get('state')))
		logging.info('----------------------------------------------------')
		for key, value in channel.json.items():
			logging.info("{}: {}".format(key, value))
					
	def run(self):
		#launch listner
		self.client.on_channel_event('StasisStart', self.on_start)
		self.client.on_channel_event('ChannelStateChange',self.channel_state_change)
		#Run the WebSocket
		if self.name_apps != '':
			self.client.run(apps=self.name_apps)
		else:
			logging.error("name of apps not set")



